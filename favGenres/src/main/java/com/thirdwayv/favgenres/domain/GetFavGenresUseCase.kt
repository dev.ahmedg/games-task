package com.thirdwayv.favgenres.domain

import com.thirdwayv.data.repository.genres.GenresRepo


class GetFavGenresUseCase constructor(private val repository: GenresRepo) {

    operator fun invoke() = repository.getFavGenres()

}