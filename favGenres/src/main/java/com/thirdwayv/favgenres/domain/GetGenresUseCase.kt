package com.thirdwayv.favgenres.domain

import com.thirdwayv.data.repository.genres.GenresRepo


class GetGenresUseCase constructor(private val repository: GenresRepo) {

    operator fun invoke() = repository.getGenres()

}