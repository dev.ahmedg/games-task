package com.thirdwayv.favgenres.domain

import com.thirdwayv.data.repository.genres.GenresRepo


class UpdateFavGenresUseCase constructor(private val repository: GenresRepo) {

    operator fun invoke(list: List<Int>) = repository.updateFavouriteGenres(list)

}