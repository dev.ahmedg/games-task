package com.thirdwayv.favgenres.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.ViewDataBinding
import androidx.paging.DifferCallback
import androidx.recyclerview.widget.DiffUtil
import com.thirdwayv.base.baseEntities.BaseEntity
import com.thirdwayv.base.baseEntities.BasePagedListAdapter
import com.thirdwayv.data.network.entities.GenreItem
import com.thirdwayv.favgenres.databinding.ItemGenreBinding

class GenresPagedAdapter: BasePagedListAdapter<GenreItem>(object : DiffUtil.ItemCallback<GenreItem>() {

    override fun areItemsTheSame(oldItem: GenreItem, newItem: GenreItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: GenreItem, newItem: GenreItem): Boolean {
        return false
    }

}) {


    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return ItemGenreBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        val item = getItem(position) ?:return
        (binding as ItemGenreBinding).apply {
            model = item

            ivSelected.isVisible =  item.isFav

            onClick = View.OnClickListener {
                item.isFav = !item.isFav

                notifyItemChanged(position)
            }
        }
    }

    fun getSelectedItems() = snapshot().filter { it?.isFav == true }.map { it?.id!! }
}