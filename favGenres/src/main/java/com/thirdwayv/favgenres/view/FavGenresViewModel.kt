package com.thirdwayv.favgenres.view

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thirdwayv.data.other.ResultState
import com.thirdwayv.favgenres.domain.GetGenresUseCase
import com.thirdwayv.favgenres.domain.UpdateFavGenresUseCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class FavGenresViewModel(getGenresUseCase: GetGenresUseCase, private val updateFavGenresUseCase: UpdateFavGenresUseCase
):ViewModel() {


    private val _setFavGenreState = MutableLiveData<ResultState<Nothing>>()

    val setFavGenreState = _setFavGenreState

    val allGenresPaged = getGenresUseCase()


    fun updateFavGenres(list: List<Int>) {
        viewModelScope.launch {
            updateFavGenresUseCase(list).collect {
                _setFavGenreState.value = it
            }
        }
    }

}