package com.thirdwayv.favgenres.view

import androidx.lifecycle.lifecycleScope
import com.thirdwayv.base.baseEntities.BaseActivity
import com.thirdwayv.base.baseEntities.LoaderStateAdapter
import com.thirdwayv.base.utils.handleInitialState
import com.thirdwayv.data.other.ResultState
import com.thirdwayv.favgenres.R
import com.thirdwayv.favgenres.databinding.ActivityFavGenresBinding
import com.thirdwayv.gameslist.navigation.GamesListNavigation
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavGenresActivity : BaseActivity<ActivityFavGenresBinding>() {


    private val mViewModel by viewModel<FavGenresViewModel>()

    private val genresAdapter = GenresPagedAdapter()

    private val genresLoadStateAdapter = LoaderStateAdapter { genresAdapter.retry() }

    override fun getLayoutRes(): Int  = R.layout.activity_fav_genres

    override fun getToolbarTitle(): Any  = R.string.select_genres

    override fun onViewAttach() {
        super.onViewAttach()

        setUpoOnClickListeners()
        setUpRv()
        observeData()

    }

    private fun setUpoOnClickListeners() {
        mBinding.btnNext.setOnClickListener {
            val selected = genresAdapter.getSelectedItems()
            if (selected.isNotEmpty()) mViewModel.updateFavGenres(selected)
        }
    }

    private fun observeData() {

        mViewModel.setFavGenreState.observe(this){
            if (it is ResultState.Success){
                navToGames()
            }
        }

        lifecycleScope.launch {
            mViewModel.allGenresPaged.distinctUntilChanged().collectLatest {
                genresAdapter.submitData(it)
            }
        }

        genresAdapter.handleInitialState(
            onLoading = {showLoading(mBinding.stateful)},
            onEmpty = {showEmpty(mBinding.stateful)},
            onError = {showError(mBinding.stateful,it.message){genresAdapter.retry()}},
            resetStates = {showContent(mBinding.stateful)}
        )
    }

    private fun navToGames() {
        GamesListNavigation().startListScreen(true)
    }

    private fun setUpRv() {
        mBinding.rv.adapter = genresAdapter.withLoadStateFooter(genresLoadStateAdapter)
    }
}