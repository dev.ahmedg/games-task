package com.thirdwayv.favgenres.navigation

import com.thirdwayv.base.baseEntities.BaseNavigationManager
import com.thirdwayv.favgenres.view.FavGenresActivity
import com.thirdwayv.gameslist.view.GamesListActivity

class FavGenresNavigation : BaseNavigationManager(){

    fun startFavScreen(clearStack:Boolean = false) {
        start(FavGenresActivity::class.java,clearStack)
    }


}