package com.thirdwayv.favgenres.di



import com.thirdwayv.favgenres.domain.GetFavGenresUseCase
import com.thirdwayv.favgenres.domain.GetGenresUseCase
import com.thirdwayv.favgenres.domain.UpdateFavGenresUseCase
import com.thirdwayv.favgenres.view.FavGenresViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val favGenresModule = module {

    factory { GetFavGenresUseCase(get())}

    factory { GetGenresUseCase(get()) }

    factory { UpdateFavGenresUseCase(get()) }

    viewModel { FavGenresViewModel(get(),get())}
}