package com.thirdwayv.gamestask.repository.games

import android.content.Context
import com.thirdwayv.base.utils.NetworkHelper
import com.thirdwayv.data.R
import com.thirdwayv.data.keyValue.KeyValueStore
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.data.network.entities.ScreenShotItem
import com.thirdwayv.data.network.entities.TrailerItem
import com.thirdwayv.data.other.ResultState
import com.thirdwayv.data.repository.games.GamesLocalDataSource
import com.thirdwayv.data.repository.games.GamesRemoteDataSource
import com.thirdwayv.data.repository.games.GamesRepoImpl
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class GamesRepoImplTest{

    private lateinit var gamesRepo: GamesRepoImpl

    @Mock
    private lateinit var localDataSource: GamesLocalDataSource
    @Mock
    private lateinit var remoteDataSource: GamesRemoteDataSource
    @Mock
    private lateinit var context: Context
    @Mock
    private lateinit var keyValueStore: KeyValueStore
    @Mock
    private lateinit var  networkHelper: NetworkHelper



    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        gamesRepo = GamesRepoImpl(context, networkHelper, localDataSource, remoteDataSource, keyValueStore)
    }


    @Test
    fun testGetGameDetailsWhenConnectedAndCachedData(): Unit = runBlocking {

        val gameItem = Mockito.mock(GameItem::class.java)
        val trailers = listOf(Mockito.mock(TrailerItem::class.java))
        val screenShots = listOf(Mockito.mock(ScreenShotItem::class.java))

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)

        Mockito.`when`(remoteDataSource.getGameDetails(anyInt()))
            .thenReturn(gameItem)

        Mockito.`when`(remoteDataSource.getGameTrailers(anyInt()))
            .thenReturn(trailers)

        Mockito.`when`(remoteDataSource.getScreenShots(anyInt()))
            .thenReturn(screenShots)

        Mockito.`when`(localDataSource.getGameDetails(anyInt()))
            .thenReturn(gameItem)


        val allEmits = gamesRepo.getGameDetails(anyInt()).toList()

        Assert.assertEquals(3,allEmits.size)

        allEmits[0] shouldBeInstanceOf ResultState.Loading::class.java
        allEmits[1] shouldBeInstanceOf ResultState.Success::class.java
        allEmits[2] shouldBeInstanceOf ResultState.Success::class.java

        (allEmits[1] as ResultState.Success).data shouldBeEqualTo gameItem
        (allEmits[2] as ResultState.Success).data shouldBeEqualTo gameItem

        Mockito.verify(localDataSource, Mockito.times(1))
            .getGameDetails(anyInt())

        Mockito.verify(localDataSource, Mockito.times(3))
            .updateGameItem(gameItem)

        Mockito.verify(remoteDataSource, Mockito.times(1))
            .getGameDetails(anyInt())

        Mockito.verify(remoteDataSource, Mockito.times(1))
            .getGameTrailers(anyInt())

        Mockito.verify(remoteDataSource, Mockito.times(1))
            .getScreenShots(anyInt())

        gameItem.screenShots shouldBeEqualTo screenShots
        gameItem.trailers shouldBeEqualTo trailers
    }


    @Test
    fun testGetGameDetailsWhenDisconnectedAndNotCachedData(): Unit = runBlocking {

        val error = "No internet connection"

        Mockito.`when`(context.getString(R.string.no_internet_connection))
            .thenReturn(error)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(false)

        Mockito.`when`(localDataSource.getGameDetails(anyInt()))
            .thenReturn(null)

        val allEmits = gamesRepo.getGameDetails(anyInt()).toList()

        Assert.assertEquals(allEmits.size,2)

        allEmits[0] shouldBeInstanceOf ResultState.Loading::class.java
        allEmits[1] shouldBeInstanceOf ResultState.Error::class.java
        ( allEmits[1] as ResultState.Error ).exception.message shouldBeEqualTo error

        Mockito.verify(remoteDataSource, Mockito.never())
            .getGameDetails(anyInt())

    }

    @Test
    fun testGetGameDetailsWhenDisconnectedAndCachedData(): Unit = runBlocking {

        val gameItem = Mockito.mock(GameItem::class.java)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(false)

        Mockito.`when`(localDataSource.getGameDetails(anyInt()))
            .thenReturn(gameItem)

        val allEmits = gamesRepo.getGameDetails(anyInt()).toList()

        Assert.assertEquals(2,allEmits.size)

        allEmits[0] shouldBeInstanceOf ResultState.Loading::class.java
        allEmits[1] shouldBeInstanceOf ResultState.Success::class.java
        (allEmits[1] as ResultState.Success).data shouldBeEqualTo gameItem

        Mockito.verify(localDataSource, Mockito.times(1))
            .getGameDetails(anyInt())


    }
}