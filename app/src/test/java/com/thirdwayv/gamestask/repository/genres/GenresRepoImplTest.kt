package com.thirdwayv.gamestask.repository.genres

import android.content.Context
import com.thirdwayv.base.utils.NetworkHelper
import com.thirdwayv.data.keyValue.KeyValueStore
import com.thirdwayv.data.other.ResultState
import com.thirdwayv.data.repository.genres.GenresLocalDataSource
import com.thirdwayv.data.repository.genres.GenresRemoteDataSource
import com.thirdwayv.data.repository.genres.GenresRepoImpl
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class GenresRepoImplTest{

    private lateinit var genresRepo: GenresRepoImpl

    @Mock
    private lateinit var localDataSource: GenresLocalDataSource
    @Mock
    private lateinit var remoteDataSource: GenresRemoteDataSource
    @Mock
    private lateinit var context: Context
    @Mock
    private lateinit var keyValueStore: KeyValueStore
    @Mock
    private lateinit var  networkHelper: NetworkHelper



    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        genresRepo = GenresRepoImpl(context, localDataSource, remoteDataSource, keyValueStore, networkHelper)
    }


    @Test
    fun testGetFavGenres(): Unit = runBlocking {

        val data = listOf(30)


        Mockito.`when`(keyValueStore.getFavGenres())
            .thenReturn(data)

        val emitted = genresRepo.getFavGenres().first()

        emitted shouldBeEqualTo data

        Mockito.verify(keyValueStore, Mockito.times(1))
            .getFavGenres()

    }


    @Test
    fun testUpdateFavouriteGenresNoUpdate(): Unit = runBlocking {

        val newList = listOf(1,50)

        Mockito.`when`(keyValueStore.getFavGenres())
            .thenReturn(newList)

        val emitted = genresRepo.updateFavouriteGenres(newList).first()

        emitted shouldBeInstanceOf ResultState.Success::class

        Mockito.verify(keyValueStore, Mockito.times(1))
            .getFavGenres()

        Mockito.verify(keyValueStore, Mockito.times(1))
            .setIsFavGenresSet(true)

        Mockito.verifyNoInteractions(localDataSource)

    }

    @Test
    fun testUpdateFavouriteGenresWithUpdate(): Unit = runBlocking {

        val newList = listOf(1,50)

        Mockito.`when`(keyValueStore.getFavGenres())
            .thenReturn(listOf(50))

        val emitted = genresRepo.updateFavouriteGenres(newList).first()

        emitted shouldBeInstanceOf ResultState.Success::class

        Mockito.verify(keyValueStore, Mockito.times(1))
            .saveFavGenres(newList)

        Mockito.verify(localDataSource, Mockito.times(1))
            .removeAllGames()

    }


    @Test
    fun testIsFavGenresSet(){

        Mockito.`when`(keyValueStore.getIsFavGenresSet())
            .thenReturn(false)

        val isSet = genresRepo.isFavGenresSet()
        isSet shouldBeEqualTo false

        Mockito.verify(keyValueStore, Mockito.times(1))
            .getIsFavGenresSet()
    }

}