package com.thirdwayv.gamestask.gamedetails.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.thirdwayv.base.utils.getOrAwaitValue
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.data.other.ResultState
import com.thirdwayv.data.repository.games.GamesRepo
import com.thirdwayv.gamedetails.domain.GetGameDetailsUseCase
import com.thirdwayv.gamedetails.view.GameDetailsUiModel
import com.thirdwayv.gamedetails.view.GameDetailsViewModel
import com.thirdwayv.gamestask.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


@RunWith(JUnit4::class)
class GameDetailsViewModelTest{

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Mock
    lateinit var repository: GamesRepo

    private lateinit var gameDetailsViewModel: GameDetailsViewModel


    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testGetData() :Unit  = runBlocking{

        val result = Mockito.mock(GameItem::class.java)

        Mockito.`when`(repository.getGameDetails(anyInt()))
            .thenReturn(flowOf(ResultState.Success(result)))

        gameDetailsViewModel = GameDetailsViewModel(anyInt(), GetGameDetailsUseCase(repository))

        gameDetailsViewModel.detailsLiveData.observeForever {  }

        val observedValue = gameDetailsViewModel.detailsLiveData.getOrAwaitValue()

        observedValue shouldBeInstanceOf ResultState.Success::class.java
        (observedValue as ResultState.Success).data shouldBeInstanceOf GameDetailsUiModel::class.java

    }

}