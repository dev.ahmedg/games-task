package com.thirdwayv.gamestask.splash.view

import com.thirdwayv.data.repository.genres.GenresRepo
import com.thirdwayv.splash.domain.FavouriteGenresIsSetUseCase
import com.thirdwayv.splash.view.SplashViewModel
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class SplashViewModelTest{


    @Mock
    lateinit var repository: GenresRepo

    private lateinit var splashViewModel: SplashViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        val favouriteGenresIsSetUseCase = FavouriteGenresIsSetUseCase(repository)

        splashViewModel = SplashViewModel(favouriteGenresIsSetUseCase)
    }

    @Test
    fun testIsFavSet() {

        Mockito.`when`(repository.isFavGenresSet())
            .thenReturn(false)

        val isFavSet = splashViewModel.isFavSet()

        isFavSet shouldBeEqualTo false
    }

}