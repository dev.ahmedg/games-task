package com.thirdwayv.gamestask.favgenres.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.thirdwayv.base.utils.getOrAwaitValue
import com.thirdwayv.data.other.ResultState
import com.thirdwayv.data.repository.genres.GenresRepo
import com.thirdwayv.favgenres.domain.GetGenresUseCase
import com.thirdwayv.favgenres.domain.UpdateFavGenresUseCase
import com.thirdwayv.favgenres.view.FavGenresViewModel
import com.thirdwayv.gamestask.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyList
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


@RunWith(JUnit4::class)
class FavGenresViewModelTest{

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Mock
    lateinit var repository: GenresRepo

    private lateinit var favGenresViewModel: FavGenresViewModel

    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)
        favGenresViewModel = FavGenresViewModel(GetGenresUseCase(repository), UpdateFavGenresUseCase(repository))
    }

    @Test
    fun testUpdateFavGenres() :Unit  = runBlocking{

        Mockito.`when`(repository.updateFavouriteGenres(anyList()))
            .thenReturn(flowOf(ResultState.Success(null)))

        favGenresViewModel.updateFavGenres(anyList())

        favGenresViewModel.setFavGenreState.observeForever {  }

        favGenresViewModel.setFavGenreState.getOrAwaitValue() shouldBeInstanceOf  ResultState.Success::class.java

    }

}