package com.thirdwayv.gamestask.app.repository.games

import android.content.Context
import androidx.paging.*
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.thirdwayv.base.utils.NetworkHelper
import com.thirdwayv.base.utils.jsonAssetFileToArr
import com.thirdwayv.data.constant.DEFAULT_PAGE_SIZE
import com.thirdwayv.data.database.AppDatabase
import com.thirdwayv.data.keyValue.KeyValueStore
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.data.network.entities.PaginationResponse
import com.thirdwayv.data.repository.games.GamesLocalDataSource
import com.thirdwayv.data.repository.games.GamesLocalDataSourceRoomImpl
import com.thirdwayv.data.repository.games.GamesPagingMediator
import com.thirdwayv.data.repository.games.GamesRemoteDataSource
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.io.IOException

@ExperimentalPagingApi
@RunWith(AndroidJUnit4::class)
class GamesPagingMediatorTest {

    private lateinit var remoteMediator: GamesPagingMediator
    private lateinit var appDatabase: AppDatabase
    private lateinit var localDataSource: GamesLocalDataSource


    @Mock
    private lateinit var remoteDataSource: GamesRemoteDataSource
    @Mock
    private lateinit var networkHelper: NetworkHelper
    @Mock
    private lateinit var keyValueStore: KeyValueStore


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        localDataSource = GamesLocalDataSourceRoomImpl(appDatabase)

        remoteMediator = GamesPagingMediator(context,networkHelper,localDataSource,remoteDataSource,keyValueStore)
    }



    @Test
    fun refreshLoadReturnsSuccessResultWhenMoreDataIsPresent() = runBlocking {

        val data = jsonAssetFileToArr<GameItem>(getTestContext(),"games_list_page.json")!!

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)

        Mockito.`when`(remoteDataSource.getGames(
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyList()
        ))
            .thenReturn(PaginationResponse(results = data, next = ""))

        val pagingState = PagingState<Int, GameItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue( result is RemoteMediator.MediatorResult.Success )
        assertFalse( (result as RemoteMediator.MediatorResult.Success).endOfPaginationReached  )
    }



    @Test
    fun refreshLoadSuccessAndEndOfPaginationWhenNoMoreData() = runBlocking {

        val data = jsonAssetFileToArr<GameItem>(getTestContext(),"games_list_page.json")!!

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)


        Mockito.`when`(remoteDataSource.getGames(
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyList(),
        ))
            .thenReturn(PaginationResponse(results = data ))

        val pagingState = PagingState<Int, GameItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue(result is RemoteMediator.MediatorResult.Success)
        assertTrue((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached)
    }

    @Test
    fun refreshLoadReturnsNetworkErrorResultWhenInternetIsNotAvailableOccurs() = runBlocking {

        val error = "No internet connection"

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(false)

        val pagingState = PagingState<Int, GameItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue(result is RemoteMediator.MediatorResult.Error)
        assertTrue((result as RemoteMediator.MediatorResult.Error).throwable.message == error)
    }


    @Test
    fun refreshLoadReturnsErrorResultWhenErrorOccurs() = runBlocking {

        val exception = Exception()

        Mockito.`when`(remoteDataSource.getGames(
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyList()
        ))
            .thenThrow(exception)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)

        val pagingState = PagingState<Int, GameItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue(result is RemoteMediator.MediatorResult.Error && result.throwable == exception)
    }


    private fun getTestContext(): Context = InstrumentationRegistry.getInstrumentation().context

    @After
    @Throws(IOException::class)
    fun closeDb() {
        appDatabase.clearAllTables()

    }
}