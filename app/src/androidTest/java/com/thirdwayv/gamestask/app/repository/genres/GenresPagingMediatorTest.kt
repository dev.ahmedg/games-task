package com.thirdwayv.gamestask.app.repository.genres

import android.content.Context
import androidx.paging.*
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.thirdwayv.base.utils.NetworkHelper
import com.thirdwayv.base.utils.jsonAssetFileToArr
import com.thirdwayv.data.constant.DEFAULT_PAGE_SIZE
import com.thirdwayv.data.database.AppDatabase
import com.thirdwayv.data.keyValue.KeyValueStore
import com.thirdwayv.data.network.entities.GenreItem
import com.thirdwayv.data.network.entities.PaginationResponse
import com.thirdwayv.data.repository.genres.GenresLocalDataSource
import com.thirdwayv.data.repository.genres.GenresLocalDataSourceRoomImpl
import com.thirdwayv.data.repository.genres.GenresPagingMediator
import com.thirdwayv.data.repository.genres.GenresRemoteDataSource
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.io.IOException

@ExperimentalPagingApi
@RunWith(AndroidJUnit4::class)
class GenresPagingMediatorTest{


    private lateinit var remoteMediator: GenresPagingMediator
    private lateinit var appDatabase: AppDatabase
    private lateinit var localDataSource: GenresLocalDataSource


    @Mock
    private lateinit var remoteDataSource: GenresRemoteDataSource
    @Mock
    private lateinit var networkHelper: NetworkHelper
    @Mock
    private lateinit var keyValueStore: KeyValueStore


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        localDataSource = GenresLocalDataSourceRoomImpl(appDatabase)

        remoteMediator = GenresPagingMediator(context,networkHelper,localDataSource,remoteDataSource,keyValueStore)
    }



    @Test
    fun refreshLoadReturnsSuccessResultWhenMoreDataIsPresent() = runBlocking {

        val data = jsonAssetFileToArr<GenreItem>(getTestContext(),"genre_list_page.json")!!

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)

        Mockito.`when`(remoteDataSource.getGenres(
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyInt()
        ))
            .thenReturn(PaginationResponse(results = data, next = ""))

        val pagingState = PagingState<Int, GenreItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue( result is RemoteMediator.MediatorResult.Success )
        assertFalse( (result as RemoteMediator.MediatorResult.Success).endOfPaginationReached  )
    }



    @Test
    fun refreshLoadSuccessAndEndOfPaginationWhenNoMoreData() = runBlocking {

        val data = jsonAssetFileToArr<GenreItem>(getTestContext(),"genre_list_page.json")!!

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)


        Mockito.`when`(remoteDataSource.getGenres(
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyInt()
        ))
            .thenReturn(PaginationResponse(results = data ))

        val pagingState = PagingState<Int, GenreItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue(result is RemoteMediator.MediatorResult.Success)
        assertTrue((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached)
    }

    @Test
    fun refreshLoadReturnsNetworkErrorResultWhenInternetIsNotAvailableOccurs() = runBlocking {

        val error = "No internet connection"

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(false)

        val pagingState = PagingState<Int, GenreItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue(result is RemoteMediator.MediatorResult.Error)
        assertTrue((result as RemoteMediator.MediatorResult.Error).throwable.message == error)
    }


    @Test
    fun refreshLoadReturnsErrorResultWhenErrorOccurs() = runBlocking {

        val exception = Exception()

        Mockito.`when`(remoteDataSource.getGenres(
            ArgumentMatchers.anyInt(),
            ArgumentMatchers.anyInt()
        ))
            .thenThrow(exception)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)

        val pagingState = PagingState<Int, GenreItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue(result is RemoteMediator.MediatorResult.Error && result.throwable == exception)
    }


    private fun getTestContext(): Context = InstrumentationRegistry.getInstrumentation().context

    @After
    @Throws(IOException::class)
    fun closeDb() {
        appDatabase.clearAllTables()

    }
}