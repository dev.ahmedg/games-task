package com.thirdwayv.gamestask.app

import androidx.multidex.MultiDexApplication
import com.orhanobut.hawk.Hawk
import com.thirdwayv.gamestask.di.allFeatures
import com.thirdwayv.base.di.utilsModule
import com.thirdwayv.base.events.ShouldOpenFavGenresScreenEvent
import com.thirdwayv.data.di.databaseModule
import com.thirdwayv.data.di.networkModule
import com.thirdwayv.data.di.repositoryModule
import com.thirdwayv.favgenres.navigation.FavGenresNavigation
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * Custom app class to initialize koin dependencies graph
 */
class ThirdWayvApp : MultiDexApplication() {


    override fun onCreate() {
        super.onCreate()
        initHawk()
        initKoin()
        registerEventBus()
    }

    private fun registerEventBus() {
        EventBus.getDefault().register(this)
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@ThirdWayvApp)
            val modules = ArrayList(allFeatures)
            modules.addAll(listOf(databaseModule, utilsModule ,networkModule, repositoryModule))
            modules(
                modules
            )
        }
    }

    private fun initHawk() {
        Hawk.init(this).build();
    }


    @Subscribe
    fun openFavGenresScreen(event : ShouldOpenFavGenresScreenEvent){
        FavGenresNavigation().startFavScreen()
    }
}