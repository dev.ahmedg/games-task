package com.thirdwayv.gamestask.di

import com.thirdwayv.favgenres.di.favGenresModule
import com.thirdwayv.gamedetails.di.gameDetailsModule
import com.thirdwayv.gameslist.di.gamesListModule
import com.thirdwayv.splash.di.splashModule


var allFeatures = listOf(
    favGenresModule,
    gamesListModule,
    splashModule,
    gameDetailsModule,
)
