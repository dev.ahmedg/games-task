package com.thirdwayv.splash.view

import android.os.Handler
import android.os.Looper
import com.thirdwayv.base.baseEntities.BaseActivity
import com.thirdwayv.favgenres.navigation.FavGenresNavigation
import com.thirdwayv.gameslist.navigation.GamesListNavigation
import com.thirdwayv.splash.R
import com.thirdwayv.splash.databinding.ActivitySplashBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    private val mViewModel by viewModel<SplashViewModel>()

    override fun getLayoutRes(): Int = R.layout.activity_splash

    override fun getToolbarTitle(): Any?  = null

    override fun onViewAttach() {
        super.onViewAttach()
        navAfterSleep()
    }

    private fun navAfterSleep() {
        Handler(Looper.getMainLooper())
            .postDelayed({
                if (mViewModel.isFavSet()){
                    GamesListNavigation().startListScreen(true)
                }else{
                    FavGenresNavigation().startFavScreen(true)
                }
            }, 2000)
    }
}