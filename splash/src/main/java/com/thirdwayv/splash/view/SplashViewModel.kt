package com.thirdwayv.splash.view

import androidx.lifecycle.ViewModel
import com.thirdwayv.splash.domain.FavouriteGenresIsSetUseCase


class SplashViewModel(private val favouriteGenresIsSetUseCase : FavouriteGenresIsSetUseCase):ViewModel() {


    fun isFavSet() = favouriteGenresIsSetUseCase()
}