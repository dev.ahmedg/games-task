package com.thirdwayv.splash.domain

import com.thirdwayv.data.repository.genres.GenresRepo


class FavouriteGenresIsSetUseCase constructor(private val repository: GenresRepo) {

    operator fun invoke() = repository.isFavGenresSet()

}