package com.thirdwayv.splash.di



import com.thirdwayv.splash.domain.FavouriteGenresIsSetUseCase
import com.thirdwayv.splash.view.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val splashModule = module {

    factory { FavouriteGenresIsSetUseCase(get()) }

    viewModel { SplashViewModel(get())}
}