package com.thirdwayv.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey


/**
 * Room entity class used to store [prevKey] and [nextKey] of each loaded game item to help determining api append / prepend in games paginated recycler view
 */
@Entity(tableName = "game_remote_key")
data class GameRemoteKey(@PrimaryKey val id: Int, val prevKey: Int?, val nextKey: Int?)
