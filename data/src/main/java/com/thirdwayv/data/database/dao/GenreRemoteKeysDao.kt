package com.thirdwayv.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.thirdwayv.data.database.entity.GenreRemoteKey

@Dao
interface GenreRemoteKeysDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<GenreRemoteKey>)

    @Query("SELECT * FROM genre_remote_key WHERE id = :id")
    suspend fun remoteKeyById(id: Int): GenreRemoteKey?

    @Query("DELETE FROM genre_remote_key")
    suspend fun clearRemoteKeys()
}

