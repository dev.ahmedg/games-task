package com.thirdwayv.data.database.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.thirdwayv.data.network.entities.GenreItem

@Dao
interface GenreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(models: List<GenreItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(model: GenreItem)

    @Query("SELECT * FROM genre")
    fun getAll(): PagingSource<Int, GenreItem>

    @Query("DELETE FROM genre")
    suspend fun clearAll()



}