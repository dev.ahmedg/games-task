package com.thirdwayv.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.thirdwayv.data.database.entity.GameRemoteKey
import com.thirdwayv.data.database.entity.GenreRemoteKey

@Dao
interface GameRemoteKeysDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<GameRemoteKey>)

    @Query("SELECT * FROM game_remote_key WHERE id = :id")
    suspend fun remoteKeyById(id: Int): GameRemoteKey?

    @Query("DELETE FROM game_remote_key")
    suspend fun clearRemoteKeys()
}

