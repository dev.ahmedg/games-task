package com.thirdwayv.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Room entity class used to store [prevKey] and [nextKey] of each loaded genre item to help determining api append / prepend in genres paginated recycler view
 */
@Entity(tableName = "genre_remote_key")
data class GenreRemoteKey(@PrimaryKey val id: Int, val prevKey: Int?, val nextKey: Int?)
