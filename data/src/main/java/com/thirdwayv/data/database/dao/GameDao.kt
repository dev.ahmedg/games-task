package com.thirdwayv.data.database.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.data.network.entities.GenreItem

@Dao
interface GameDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(models: List<GameItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(model: GameItem)

    @Query("SELECT * FROM game ORDER BY added DESC")
    fun getAll(): PagingSource<Int, GameItem>

    @Query("DELETE FROM game")
    suspend fun clearAll()

    @Query("SELECT * FROM game WHERE id = :id")
    suspend fun getById(id: Int): GameItem


}