package com.thirdwayv.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.thirdwayv.base.utils.fromJson
import com.thirdwayv.base.utils.fromJsonArray
import com.thirdwayv.base.utils.toJson
import com.thirdwayv.base.utils.toJsonArray
import com.thirdwayv.data.database.dao.GameDao
import com.thirdwayv.data.database.dao.GameRemoteKeysDao
import com.thirdwayv.data.database.dao.GenreDao
import com.thirdwayv.data.database.dao.GenreRemoteKeysDao
import com.thirdwayv.data.database.entity.GameRemoteKey
import com.thirdwayv.data.database.entity.GenreRemoteKey
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.data.network.entities.GenreItem
import com.thirdwayv.data.network.entities.ScreenShotItem
import com.thirdwayv.data.network.entities.TrailerItem

/**
 * Room database class
 */
@Database(
    entities = [GenreItem::class,GameItem::class,GenreRemoteKey::class,GameRemoteKey::class],
    version = 1
)
@TypeConverters(DataConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun gameDao() : GameDao
    abstract fun genreDao() : GenreDao
    abstract fun genreRemoteKeysDao() : GenreRemoteKeysDao
    abstract fun gameRemoteKeysDao() : GameRemoteKeysDao


}

class DataConverter{

    @TypeConverter
    fun toTrailerList(json: String?): List<TrailerItem>? = fromJsonArray(json)

    @TypeConverter
    fun fromTrailerList(list: List<TrailerItem>?) = toJsonArray(list)

    @TypeConverter
    fun toScreenShotList(json: String?): List<ScreenShotItem>? = fromJsonArray(json)

    @TypeConverter
    fun fromScreenShotList(list: List<ScreenShotItem>?) = toJsonArray(list)

}
