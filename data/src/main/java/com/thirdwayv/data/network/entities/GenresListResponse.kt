package com.thirdwayv.data.network.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.thirdwayv.base.baseEntities.BaseEntity

@Entity(tableName = "genre")
data class GenreItem(

	@field:SerializedName("games_count")
	val gamesCount: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@PrimaryKey
	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("image_background")
	val imageBackground: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null,

	var isFav : Boolean = false

):BaseEntity {
	override fun entityId(): Int = id
}

