package com.thirdwayv.data.network.api


import com.thirdwayv.data.constant.GENRES_LIST_API
import com.thirdwayv.data.network.entities.GenreItem
import com.thirdwayv.data.network.entities.PaginationResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GenreApiService {

    @GET(GENRES_LIST_API)
    suspend fun getGenres(@Query("page") page: Int, @Query("page_size") pageSize: Int , @Query("ordering")orderBy :String = "id"): Response<PaginationResponse<GenreItem>>



}