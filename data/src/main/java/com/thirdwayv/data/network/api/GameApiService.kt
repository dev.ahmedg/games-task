package com.thirdwayv.data.network.api


import com.thirdwayv.data.constant.GAMES_LIST_API
import com.thirdwayv.data.constant.GAME_DETAILS_API
import com.thirdwayv.data.constant.GAME_SCREEN_SHOTS_LIST_API
import com.thirdwayv.data.constant.GAME_TRAILERS_LIST_API
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.data.network.entities.PaginationResponse
import com.thirdwayv.data.network.entities.ScreenShotItem
import com.thirdwayv.data.network.entities.TrailerItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GameApiService {

    @GET(GAMES_LIST_API)
    suspend fun getGames(@Query("page") page: Int, @Query("page_size") pageSize: Int, @Query("genres")genres:String): Response<PaginationResponse<GameItem>>

    @GET(GAME_TRAILERS_LIST_API)
    suspend fun getGameTrailers(@Path("id") id: Int): Response<PaginationResponse<TrailerItem>>

    @GET(GAME_SCREEN_SHOTS_LIST_API)
    suspend fun getGameScreenShots(@Path("id") id: Int): Response<PaginationResponse<ScreenShotItem>>

    @GET(GAME_DETAILS_API)
    suspend fun getGameDetails(@Path("id")id:Int): Response<GameItem>


}