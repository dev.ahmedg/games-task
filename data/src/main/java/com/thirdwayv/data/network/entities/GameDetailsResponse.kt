package com.thirdwayv.data.network.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.thirdwayv.base.baseEntities.BaseEntity

@Entity(tableName = "game")
open class GameItem(

	@field:SerializedName("added")
	val added: Int?,

	var trailers: List<TrailerItem>?,

	var screenShots: List<ScreenShotItem>?,

	@field:SerializedName("name_original")
	val nameOriginal: String?,

	@field:SerializedName("rating")
	val rating: Double?,

	@field:SerializedName("playtime")
	val playtime: Int?,

	@field:SerializedName("rating_top")
	val ratingTop: Int?,

	@field:SerializedName("reviews_text_count")
	val reviewsTextCount: Int?,

	@PrimaryKey
	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("ratings_count")
	val ratingsCount: Int?,

	@field:SerializedName("movies_count")
	val moviesCount: Int?,

	@field:SerializedName("description_raw")
	val descriptionRaw: String?,

	@field:SerializedName("background_image")
	val backgroundImage: String?,

	@field:SerializedName("name")
	val name: String?,

	@field:SerializedName("reviews_count")
	val reviewsCount: Int?,

	@field:SerializedName("description")
	val description: String?,

	@field:SerializedName("screenshots_count")
	val screenshotsCount: Int?,

	@field:SerializedName("released")
	val released: String?,


	):BaseEntity {
	override fun entityId(): Int  = id
}

open class TrailerItem(

	@field:SerializedName("preview")
	val preview: String?,

	@field:SerializedName("data")
	val data: TrailerData?,

	@field:SerializedName("name")
	val name: String?,

	@field:SerializedName("id")
	val id: Int?
)

open class TrailerData(

	@field:SerializedName("max")
	val max: String?,

	@field:SerializedName("480")
	val jsonMember480: String?
)

open class ScreenShotItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("is_deleted")
	val isDeleted: Boolean? = null,

	@field:SerializedName("width")
	val width: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("height")
	val height: Int? = null
)
