package com.thirdwayv.data.network.entities

import com.google.gson.annotations.SerializedName

/**
 * A generic container class for the paginated apis responses
 */
data class PaginationResponse<T>(

    @field:SerializedName("next")
    val next: String? = null,

    @field:SerializedName("previous")
    val previous: String? = null,

    @field:SerializedName("count")
    val count: Int? = null,

    @field:SerializedName("results")
    val results: List<T>
)