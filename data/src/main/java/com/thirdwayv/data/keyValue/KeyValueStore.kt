package com.thirdwayv.data.keyValue


/**
 * KeyValueStore protocol
 */
interface KeyValueStore {

    suspend fun saveFavGenres(list: List<Int>)
    fun getFavGenres(): List<Int>?

    suspend fun setIsFavGenresSet(isSet:Boolean)
    fun getIsFavGenresSet(): Boolean

    companion object {
        const val KEY_FAVOURITES_GENRES= "fav_genres"
        const val KEY_IS_FAV_GENRES_SET= "is_fav_genres_set"
    }
}