package com.thirdwayv.data.keyValue

import com.orhanobut.hawk.Hawk
import com.thirdwayv.data.keyValue.KeyValueStore.Companion.KEY_FAVOURITES_GENRES
import com.thirdwayv.data.keyValue.KeyValueStore.Companion.KEY_IS_FAV_GENRES_SET


/**
 * KeyValueStore implementation with Hawk library
 */
class KeyValueStoreHawkImpl : KeyValueStore {
    override suspend fun saveFavGenres(list: List<Int>) {
        Hawk.put(KEY_FAVOURITES_GENRES,list)
    }

    override fun getFavGenres(): List<Int>? {
        return Hawk.get(KEY_FAVOURITES_GENRES)
    }

    override suspend fun setIsFavGenresSet(isSet: Boolean) {
        Hawk.put(KEY_IS_FAV_GENRES_SET,isSet)
    }

    override fun getIsFavGenresSet(): Boolean {
        return Hawk.get(KEY_IS_FAV_GENRES_SET,false)
    }


}