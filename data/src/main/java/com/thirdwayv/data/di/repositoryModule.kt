package com.thirdwayv.data.di


import com.thirdwayv.data.repository.games.*
import com.thirdwayv.data.repository.genres.*
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


/**
 * Koin module for repositories , remote/local data sources an other sources
 */
val  repositoryModule = module {

    factory<GenresLocalDataSource>{ GenresLocalDataSourceRoomImpl(get()) }

    factory<GenresRemoteDataSource>{ GenresRemoteDataSourceRetrofitImpl(get(),get()) }

    factory<GenresRepo>{ GenresRepoImpl(androidContext(),get(),get(),get(),get()) }

    factory<GamesLocalDataSource>{ GamesLocalDataSourceRoomImpl(get()) }

    factory<GamesRemoteDataSource>{ GamesRemoteDataSourceRetrofitImpl(get(),get()) }

    factory<GamesRepo>{ GamesRepoImpl(androidContext(),get(),get(),get(),get()) }

}