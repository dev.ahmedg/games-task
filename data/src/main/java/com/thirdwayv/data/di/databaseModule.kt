package com.thirdwayv.data.di

import android.app.Application
import androidx.room.Room
import com.thirdwayv.data.database.AppDatabase
import com.thirdwayv.data.keyValue.KeyValueStore
import com.thirdwayv.data.keyValue.KeyValueStoreHawkImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/**
 * Koin module for room database
 */
val databaseModule = module {

    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "games_db")
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

    single { provideDatabase(androidApplication()) }

    single<KeyValueStore> { KeyValueStoreHawkImpl() }

}