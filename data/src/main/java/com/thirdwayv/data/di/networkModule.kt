package com.thirdwayv.data.di


import com.thirdwayv.base.BuildConfig
import com.thirdwayv.data.constant.API_KEY
import com.thirdwayv.data.constant.API_KEY_PARAM
import com.thirdwayv.data.constant.BASE_API_URL
import com.thirdwayv.data.constant.DEFAULT_NETWORK_TIMEOUT
import com.thirdwayv.data.network.RetrofitExecutor
import com.thirdwayv.data.network.api.GameApiService
import com.thirdwayv.data.network.api.GenreApiService
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Koin module for okHttp , retrofit and it`s api interfaces
 */
val networkModule = module(override = true) {

    fun provideOkHttpLoggingInterceptor() = HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE }


    fun provideAPiKeyInterceptor()  = Interceptor { chain ->
        val original = chain.request()
        val originalHttpUrl = original.url

        val url = originalHttpUrl.newBuilder()
            .addQueryParameter(API_KEY_PARAM, API_KEY)
            .build()

        val requestBuilder = original.newBuilder()
            .url(url)

        val request = requestBuilder.build()

        chain.proceed(request)
    }


    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor , apiKeyInterceptor: Interceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(DEFAULT_NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(DEFAULT_NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(DEFAULT_NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(apiKeyInterceptor)
        .build()
    }


    fun provideRetrofit(baseUrl:String, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }

    single(named(DI_BASE_URL)) { BASE_API_URL }

    single(named(DI_API_KEY_INTERCEPTOR)) { provideAPiKeyInterceptor() }

    single { provideOkHttpLoggingInterceptor() }

    single { provideOkHttpClient(get(),get(named(DI_API_KEY_INTERCEPTOR))) }

    single { provideRetrofit(get(named(DI_BASE_URL)),get()) }

    factory { RetrofitExecutor(androidContext()) }

    factory { get<Retrofit>().create(GameApiService::class.java) }

    factory { get<Retrofit>().create(GenreApiService::class.java) }


}

const val DI_BASE_URL = "baseUrl"
const val DI_API_KEY_INTERCEPTOR = "apiKeyInterceptor"