package com.thirdwayv.data.other

import android.content.Context
import androidx.annotation.MainThread
import com.thirdwayv.base.utils.NetworkHelper
import com.thirdwayv.data.R
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Manage remote/local data sources caching strategy providing the desired result as [ResultType]
 */
class RemoteLocalBoundResource<ResultType : Any>
@MainThread constructor(
    private val context:Context,
    private var networkHelper: NetworkHelper,
    private val remoteCall: suspend () -> ResultType,
    private val localCall: suspend () -> ResultType?,
    private val saveLocal: suspend (ResultType) -> Unit,

    ) {

    private suspend fun fetchFromNetwork(): ResultType {

        val response = remoteCall()

        saveLocal(response)

        return response
    }


    suspend fun asFlow(): Flow<ResultState<ResultType>> {
        return flow {
            emit(ResultState.Loading)

            try {

                val loadDataFromDB = localCall()
                val isLocalDataAvailable = (loadDataFromDB!=null)
                if (isLocalDataAvailable) emit(ResultState.Success(loadDataFromDB!!))

                if (networkHelper.isConnected()) {
                    emit(ResultState.Success(fetchFromNetwork()))
                }else{
                    if (!isLocalDataAvailable) emit(ResultState.Error(Exception(context.getString(R.string.no_internet_connection))))
                }

            }catch (e:Exception){
                emit(ResultState.Error(e))
            }


        }
    }



}