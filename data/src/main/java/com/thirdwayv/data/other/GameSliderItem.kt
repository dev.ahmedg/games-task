package com.thirdwayv.data.other

import com.thirdwayv.data.network.entities.ScreenShotItem
import com.thirdwayv.data.network.entities.TrailerItem

/**
 * The item data ui model for games screenshots/trailers slider
 */
data class GameSliderItem(val screenShotItem: ScreenShotItem? = null, val trailerItem: TrailerItem? = null, val type:Type){
    enum class Type{
        TRAILER,SCREENSHOT
    }
}
