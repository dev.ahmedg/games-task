package com.thirdwayv.data.other

/**
 * State management class for ui states providing [T] as the success state data type
 */
sealed class ResultState<out T : Any> {
    data class Success<out T : Any>(val data: T?) : ResultState<T>()
    data class Error(val exception: Throwable) : ResultState<Nothing>()
    object Loading : ResultState<Nothing>()
    object Empty : ResultState<Nothing>()
}