package com.thirdwayv.data.constant

import androidx.paging.PagingConfig
import com.thirdwayv.data.BuildConfig


var BASE_API_URL = BuildConfig.BASE_API_URL
const val API_KEY = BuildConfig.API_KEY
const val DEFAULT_NETWORK_TIMEOUT = 100L

const val DEFAULT_PAGE_INDEX = 1
const val DEFAULT_PAGE_SIZE = 10


const val API_KEY_PARAM = "key"

const val GENRES_LIST_API = "genres"
const val GAMES_LIST_API = "games"
const val GAME_DETAILS_API = "games/{id}"
const val GAME_TRAILERS_LIST_API = "games/{id}/movies"
const val GAME_SCREEN_SHOTS_LIST_API = "games/{id}/screenshots"

fun getDefaultPageConfig(): PagingConfig {
    return PagingConfig( enablePlaceholders = false,pageSize = DEFAULT_PAGE_SIZE)
}