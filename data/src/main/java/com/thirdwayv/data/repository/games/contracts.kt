package com.thirdwayv.data.repository.games

import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.thirdwayv.data.database.entity.GameRemoteKey
import com.thirdwayv.data.network.entities.*
import com.thirdwayv.data.other.ResultState
import kotlinx.coroutines.flow.Flow

/**
 * Games local data source protocol
 */
interface GamesLocalDataSource {

    fun getGames() : PagingSource<Int, GameItem>

    suspend fun saveData(isRefresh: Boolean, games: List<GameItem>, remoteKeys: List<GameRemoteKey>)

    suspend fun removeAllGames()

    suspend fun getRemoteKeyById(id:Int) : GameRemoteKey?

    suspend fun getGameDetails(id:Int): GameItem

    suspend fun updateGameItem(item: GameItem)
}

/**
 * Games remote data source protocol
 */
interface GamesRemoteDataSource {

    @Throws(Exception::class)
    suspend fun getGames(page: Int, pageSize: Int,genres: List<Int>): PaginationResponse<GameItem>
    suspend fun getGameTrailers(id: Int): List<TrailerItem>
    suspend fun getScreenShots(id: Int): List<ScreenShotItem>

    suspend fun getGameDetails(id:Int): GameItem

}

/**
 * Games repo protocol
 */
interface GamesRepo {

    fun getGames(): Flow<PagingData<GameItem>>
    suspend fun getGameDetails(id: Int): Flow<ResultState<GameItem>>

}