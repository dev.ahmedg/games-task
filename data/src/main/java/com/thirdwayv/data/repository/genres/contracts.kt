package com.thirdwayv.data.repository.genres

import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.thirdwayv.data.database.entity.GenreRemoteKey
import com.thirdwayv.data.network.entities.GenreItem
import com.thirdwayv.data.network.entities.PaginationResponse
import com.thirdwayv.data.other.ResultState
import kotlinx.coroutines.flow.Flow

/**
 * Genres local data source protocol
 */
interface GenresLocalDataSource {

    fun getAllGenres() : PagingSource<Int, GenreItem>

    suspend fun saveData(isRefresh: Boolean, genres: List<GenreItem>, remoteKeys: List<GenreRemoteKey>)

    suspend fun removeAlLGenres()

    suspend fun getRemoteKeyById(id:Int) : GenreRemoteKey?

    suspend fun removeAllGames()
}

/**
 * Genres remote data source protocol
 */
interface GenresRemoteDataSource {

    @Throws(Exception::class)
    suspend fun getGenres(page: Int, pageSize: Int): PaginationResponse<GenreItem>

}

/**
 * Genres repo protocol
 */
interface GenresRepo {

    fun getGenres(): Flow<PagingData<GenreItem>>
    fun getFavGenres(): Flow<List<Int>?>
    fun updateFavouriteGenres(list : List<Int>):Flow<ResultState<Nothing>>
    fun isFavGenresSet():Boolean

}