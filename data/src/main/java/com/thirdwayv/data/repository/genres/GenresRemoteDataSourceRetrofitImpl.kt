package com.thirdwayv.data.repository.genres

import com.thirdwayv.data.network.RetrofitExecutor
import com.thirdwayv.data.network.api.GenreApiService
import com.thirdwayv.data.network.entities.GenreItem
import com.thirdwayv.data.network.entities.PaginationResponse

/**
 * Genres local data source implementation with Retrofit library , [retrofitExecutor] and [apiService] are provided to access retrofit capabilities
 */
class GenresRemoteDataSourceRetrofitImpl(private val retrofitExecutor: RetrofitExecutor, private val apiService: GenreApiService) : GenresRemoteDataSource {


    override suspend fun getGenres(page: Int, pageSize: Int): PaginationResponse<GenreItem> {
        return retrofitExecutor.makeRequest { apiService.getGenres(page,pageSize) }
    }
}