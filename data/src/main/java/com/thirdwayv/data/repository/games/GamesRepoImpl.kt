package com.thirdwayv.data.repository.games

import android.content.Context
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingData
import com.thirdwayv.base.utils.NetworkHelper
import com.thirdwayv.data.constant.getDefaultPageConfig
import com.thirdwayv.data.keyValue.KeyValueStore
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.data.other.RemoteLocalBoundResource
import com.thirdwayv.data.other.ResultState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.lang.Exception

/**
 * Games repository implementation
 */
@OptIn(ExperimentalPagingApi::class)
class GamesRepoImpl(
    private val context: Context,
    private val networkHelper: NetworkHelper,
    private val localDataSource: GamesLocalDataSource,
    private val remoteDataSource: GamesRemoteDataSource,
    private val keyValueStore: KeyValueStore
) : GamesRepo {


    override fun getGames(): Flow<PagingData<GameItem>> {
        val pagingSourceFactory = { localDataSource.getGames()}
        return Pager(
            config = getDefaultPageConfig(),
            pagingSourceFactory = pagingSourceFactory,
            remoteMediator = GamesPagingMediator(context, networkHelper, localDataSource, remoteDataSource, keyValueStore)
        ).flow
    }

    override suspend fun getGameDetails(id: Int): Flow<ResultState<GameItem>> {

        val apiTrailers = try {
            remoteDataSource.getGameTrailers(id)
        }catch (e:Exception){
            e.printStackTrace()
            null
        }

        val apiScreenShots = try {
            remoteDataSource.getScreenShots(id)
        }catch (e:Exception){
            e.printStackTrace()
            null
        }

        return RemoteLocalBoundResource(context,networkHelper,
            remoteCall = { remoteDataSource.getGameDetails(id)},
            localCall = {localDataSource.getGameDetails(id)},
            saveLocal = { localDataSource.updateGameItem(it)}).asFlow().map {

                if (it is ResultState.Success){
                    if (apiTrailers!=null || apiScreenShots!=null){
                        val gameWithTrailer = it.data?.apply {
                            if (apiTrailers!=null) trailers = apiTrailers
                            if (apiScreenShots!=null) screenShots = apiScreenShots
                        }!!
                        localDataSource.updateGameItem(gameWithTrailer)

                    }
                }
            it
        }

    }


}