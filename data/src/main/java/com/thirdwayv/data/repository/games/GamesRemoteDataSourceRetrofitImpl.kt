package com.thirdwayv.data.repository.games

import com.thirdwayv.data.network.RetrofitExecutor
import com.thirdwayv.data.network.api.GameApiService
import com.thirdwayv.data.network.entities.*

/**
 * Games local data source implementation with Retrofit library , [retrofitExecutor] and [apiService] are provided to access retrofit capabilities
 */
class GamesRemoteDataSourceRetrofitImpl(private val retrofitExecutor: RetrofitExecutor, private val apiService: GameApiService) :
    GamesRemoteDataSource {


    override suspend fun getGames(page: Int, pageSize: Int,genres: List<Int>): PaginationResponse<GameItem> {
        return retrofitExecutor.makeRequest { apiService.getGames(page,pageSize,genres.joinToString()) }
    }

    override suspend fun getGameTrailers(
        id: Int
    ): List<TrailerItem> {
        return retrofitExecutor.makeRequest { apiService.getGameTrailers(id) }.results
    }

    override suspend fun getScreenShots(
        id: Int
    ): List<ScreenShotItem> {
        return retrofitExecutor.makeRequest { apiService.getGameScreenShots(id) }.results
    }

    override suspend fun getGameDetails(id: Int): GameItem {
        return retrofitExecutor.makeRequest { apiService.getGameDetails(id) }
    }


}