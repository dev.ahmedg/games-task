package com.thirdwayv.data.repository.games

import androidx.paging.PagingSource
import androidx.room.withTransaction
import com.thirdwayv.data.database.AppDatabase
import com.thirdwayv.data.database.entity.GameRemoteKey
import com.thirdwayv.data.database.entity.GenreRemoteKey
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.data.network.entities.GenreItem

/**
 * Games local data source implementation with jetpack room library , [appDatabase] is provided to access room capabilities
 */
class GamesLocalDataSourceRoomImpl(private val appDatabase: AppDatabase) : GamesLocalDataSource {


    override fun getGames(): PagingSource<Int, GameItem> {
        return appDatabase.gameDao().getAll()
    }

    override suspend fun saveData(
        isRefresh: Boolean,
        games: List<GameItem>,
        remoteKeys: List<GameRemoteKey>
    ) {
        appDatabase.withTransaction {
            if (isRefresh){
                appDatabase.gameDao().clearAll()
                appDatabase.gameRemoteKeysDao().clearRemoteKeys()
            }
            appDatabase.gameDao().insertAll(games)
            appDatabase.gameRemoteKeysDao().insertAll(remoteKeys)
        }
    }

    override suspend fun removeAllGames() {
        appDatabase.gameDao().clearAll()
    }

    override suspend fun getRemoteKeyById(id: Int): GameRemoteKey? {
        return appDatabase.gameRemoteKeysDao().remoteKeyById(id)
    }

    override suspend fun getGameDetails(id: Int): GameItem {
        return appDatabase.gameDao().getById(id)
    }

    override suspend fun updateGameItem(item: GameItem) {
        appDatabase.gameDao().insert(item)
    }


}