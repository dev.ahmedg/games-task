package com.thirdwayv.data.repository.genres

import android.content.Context
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingData
import com.thirdwayv.base.utils.NetworkHelper
import com.thirdwayv.data.constant.getDefaultPageConfig
import com.thirdwayv.data.keyValue.KeyValueStore
import com.thirdwayv.data.network.entities.GenreItem
import com.thirdwayv.data.other.ResultState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf

/**
 * Games repository implementation
 */
@OptIn(ExperimentalPagingApi::class)
class GenresRepoImpl(
    private val context: Context,
    private val localDataSource: GenresLocalDataSource,
    private val remoteDataSource: GenresRemoteDataSource,
    private val keyValueStore: KeyValueStore,
    private val networkHelper: NetworkHelper
) : GenresRepo {



    override fun getGenres(): Flow<PagingData<GenreItem>> {
        val pagingSourceFactory = { localDataSource.getAllGenres()}
        return Pager(
            config = getDefaultPageConfig(),
            pagingSourceFactory = pagingSourceFactory,
            remoteMediator = GenresPagingMediator(context, networkHelper, localDataSource, remoteDataSource, keyValueStore)
        ).flow
    }

    override fun getFavGenres(): Flow<List<Int>?> {
        return flowOf(keyValueStore.getFavGenres())
    }

    override fun updateFavouriteGenres(list: List<Int>):Flow<ResultState<Nothing>> {
        return flow {
            val oldList = keyValueStore.getFavGenres()
            val isUpdated = oldList != list

            keyValueStore.setIsFavGenresSet(true)

            if (isUpdated){
                keyValueStore.saveFavGenres(list)
                localDataSource.removeAllGames()
            }

            emit(ResultState.Success(null))
        }
    }

    override fun isFavGenresSet(): Boolean {
        return keyValueStore.getIsFavGenresSet()
    }


}