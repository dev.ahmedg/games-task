package com.thirdwayv.data.repository.genres

import androidx.paging.PagingSource
import androidx.room.withTransaction
import com.thirdwayv.data.database.AppDatabase
import com.thirdwayv.data.database.entity.GenreRemoteKey
import com.thirdwayv.data.network.entities.GenreItem

/**
 * Genres local data source implementation with jetpack room library , [appDatabase] is provided to access room capabilities
 */
class GenresLocalDataSourceRoomImpl(private val appDatabase: AppDatabase) : GenresLocalDataSource {


    override fun getAllGenres(): PagingSource<Int, GenreItem> {
        return appDatabase.genreDao().getAll()
    }

    override suspend fun saveData(
        isRefresh: Boolean,
        genres: List<GenreItem>,
        remoteKeys: List<GenreRemoteKey>
    ) {
        appDatabase.withTransaction {
            if (isRefresh){
                appDatabase.genreDao().clearAll()
                appDatabase.genreRemoteKeysDao().clearRemoteKeys()
            }
            appDatabase.genreDao().insertAll(genres)
            appDatabase.genreRemoteKeysDao().insertAll(remoteKeys)

        }
    }

    override suspend fun removeAlLGenres() {
        appDatabase.genreDao().clearAll()
    }

    override suspend fun getRemoteKeyById(id: Int): GenreRemoteKey? {
        return appDatabase.genreRemoteKeysDao().remoteKeyById(id)
    }

    override suspend fun removeAllGames() {
        appDatabase.gameDao().clearAll()
    }
}