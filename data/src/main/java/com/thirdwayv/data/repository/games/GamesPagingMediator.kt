package com.thirdwayv.data.repository.games

import android.content.Context
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.thirdwayv.base.utils.NetworkHelper
import com.thirdwayv.data.R
import com.thirdwayv.data.constant.DEFAULT_PAGE_INDEX
import com.thirdwayv.data.database.entity.GameRemoteKey
import com.thirdwayv.data.database.entity.GenreRemoteKey
import com.thirdwayv.data.keyValue.KeyValueStore
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.data.network.entities.GenreItem
import com.thirdwayv.data.repository.genres.GenresLocalDataSource
import com.thirdwayv.data.repository.genres.GenresRemoteDataSource


/**
 * Mediator for caching games paginated list from api
 */
@OptIn(ExperimentalPagingApi::class)
class GamesPagingMediator(private val context: Context, private val networkHelper: NetworkHelper,
                          private val localDataSource: GamesLocalDataSource, private val remoteDataSource: GamesRemoteDataSource,
                          private val keyValueStore: KeyValueStore
) : RemoteMediator<Int, GameItem>() {


    override suspend fun load(
        loadType: LoadType, state: PagingState<Int, GameItem>
    ): MediatorResult {

        val page = when (val pageKeyData = getKeyPageData(loadType, state)) {
            is MediatorResult.Success -> {
                return pageKeyData
            }
            else -> {
                pageKeyData as Int
            }
        }

        if (!networkHelper.isConnected())
            return MediatorResult.Error(Exception(context.getString(R.string.no_internet_connection)))

        try {

            val response = remoteDataSource.getGames(page,state.config.pageSize,keyValueStore.getFavGenres()!!)
            val isEndOfList = response.next == null

            val prevPage = if (page == DEFAULT_PAGE_INDEX) null else page - 1
            val nextPage = if (isEndOfList) null else page + 1

            val indexes = response.results.map {
                GameRemoteKey(it.id,prevPage,nextPage)
            }

            localDataSource.saveData(loadType == LoadType.REFRESH,response.results,indexes)

            return MediatorResult.Success(endOfPaginationReached = isEndOfList)

        }catch (e:Exception){
            return MediatorResult.Error(e)
        }
    }

    /**
     * this returns the page key or the final end of list success result
     */
    private suspend fun getKeyPageData(loadType: LoadType, state: PagingState<Int, GameItem>): Any {

        return when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getClosestRemoteKey(state)
                remoteKeys?.nextKey?.minus(1) ?: DEFAULT_PAGE_INDEX
            }
            LoadType.APPEND -> {
                val remoteKeys = getLastRemoteKey(state)

                // If remoteKeys is null, that means the refresh result is not in the database yet.
                // We can return Success with endOfPaginationReached = false because Paging
                // will call this method again if RemoteKeys becomes non-null.
                // If remoteKeys is NOT NULL but its nextKey is null, that means we've reached
                // the end of pagination for append.

                return remoteKeys?.nextKey
                    ?: MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
            LoadType.PREPEND -> {
                val remoteKeys = getFirstRemoteKey(state)

                // If remoteKeys is null, that means the refresh result is not in the database yet.
               return remoteKeys?.prevKey
                    ?: //end of list condition reached
                     MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
        }
    }

    /**
     * get the last remote key inserted which had the data
     */
    private suspend fun getLastRemoteKey(state: PagingState<Int, GameItem>): GameRemoteKey? {
        return state.pages
            .lastOrNull { it.data.isNotEmpty() }
            ?.data?.lastOrNull()
            ?.let { item -> localDataSource.getRemoteKeyById(item.id)}
    }

    /**
     * get the first remote key inserted which had the data
     */
    private suspend fun getFirstRemoteKey(state: PagingState<Int, GameItem>): GameRemoteKey? {
        return state.pages
            .firstOrNull() { it.data.isNotEmpty() }
            ?.data?.firstOrNull()
            ?.let { item -> localDataSource.getRemoteKeyById(item.id)}
    }

    /**
     * get the closest remote key inserted which had the data
     */
    private suspend fun getClosestRemoteKey(state: PagingState<Int, GameItem>): GameRemoteKey? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { id ->
                localDataSource.getRemoteKeyById(id)
            }
        }
    }


}