package com.thirdwayv.gameslist.di




import com.thirdwayv.gameslist.domain.GetGamesUseCase
import com.thirdwayv.gameslist.view.GamesListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val gamesListModule = module {

    factory { GetGamesUseCase(get())}


    viewModel { GamesListViewModel(get())}
}