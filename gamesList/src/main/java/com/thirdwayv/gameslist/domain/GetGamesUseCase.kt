package com.thirdwayv.gameslist.domain

import com.thirdwayv.data.repository.games.GamesRepo


class GetGamesUseCase constructor(private val repository: GamesRepo) {

    operator fun invoke() = repository.getGames()

}