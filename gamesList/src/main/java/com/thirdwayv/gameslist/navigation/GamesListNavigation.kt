package com.thirdwayv.gameslist.navigation

import com.thirdwayv.base.baseEntities.BaseNavigationManager
import com.thirdwayv.gameslist.view.GamesListActivity

class GamesListNavigation : BaseNavigationManager(){

    fun startListScreen(clearStack: Boolean = false) {
        start(GamesListActivity::class.java,clearStack)
    }


}