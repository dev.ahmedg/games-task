package com.thirdwayv.gameslist.view

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.lifecycleScope
import com.blankj.utilcode.util.KeyboardUtils
import com.thirdwayv.base.baseEntities.BaseActivity
import com.thirdwayv.base.baseEntities.LoaderStateAdapter
import com.thirdwayv.base.events.ShouldOpenFavGenresScreenEvent
import com.thirdwayv.base.utils.handleInitialState
import com.thirdwayv.gamedetails.navigation.GameDetailsNavigation
import com.thirdwayv.gameslist.R
import com.thirdwayv.gameslist.databinding.ActivityGamesListBinding
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.qualifier.named

class GamesListActivity : BaseActivity<ActivityGamesListBinding>() {



    private val mViewModel by viewModel<GamesListViewModel>()

    private lateinit var mSearchView: SearchView

    private val gamesPagedAdapter = GamesPagedAdapter{
        onItemClick(it.id)
    }

    private fun onItemClick(id: Int) {
        GameDetailsNavigation().startDetailsScreen(id)
    }

    private val searchResultsAdapter = GamesAdapter{
        onItemClick(it.id)
    }


    private val gamesLoadStateAdapter = LoaderStateAdapter { gamesPagedAdapter.retry() }

    override fun getLayoutRes(): Int  = R.layout.activity_games_list

    override fun getToolbarTitle(): Any  = R.string.games

    override fun onViewAttach() {
        super.onViewAttach()
        setUpRv()
        observeData()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        gamesPagedAdapter.refresh()
    }

    private fun observeData() {

        lifecycleScope.launch {
            mViewModel.allGamesPaged.distinctUntilChanged().collectLatest {
                gamesPagedAdapter.submitData(it)
            }
        }

        gamesPagedAdapter.handleInitialState(
            onLoading = {showLoading(mBinding.stateful)},
            onEmpty = {showEmpty(mBinding.stateful)},
            onError = {showError(mBinding.stateful,it.message){gamesPagedAdapter.retry()}},
            resetStates = {showContent(mBinding.stateful)}
        )
    }

    private fun setUpRv() {
        setRvAdapterAsPagedAdapter()
    }

    private fun setRvAdapterAsPagedAdapter() {
        mBinding.rv.adapter = gamesPagedAdapter.withLoadStateFooter(gamesLoadStateAdapter)
    }

    private fun setRvAdapterAsSearchedAdapter() {
        mBinding.rv.adapter = searchResultsAdapter
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_games_list, menu)
        val searchItem = menu.findItem(R.id.action_search)
        mSearchView = searchItem.actionView as SearchView

        mSearchView.queryHint = getString(R.string.search_hint)

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                KeyboardUtils.hideSoftInput(this@GamesListActivity)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                onNewQuery(newText)
                return true
            }

        })

        mSearchView.setOnFocusChangeListener { v, hasFocus ->
            if (!mSearchView.query.isNullOrEmpty()){
                KeyboardUtils.hideSoftInput(v)
            }else{
                if (!hasFocus)mSearchView.onActionViewCollapsed()
            }
        }

        mSearchView.maxWidth = Int.MAX_VALUE

        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.action_edit){
            EventBus.getDefault().post(ShouldOpenFavGenresScreenEvent())
            true
        }else{
            super.onOptionsItemSelected(item)
        }
    }


    private fun onNewQuery(query: String?) {
        if (query.isNullOrEmpty()) {
            resetSearch()
        } else {
            loadSearchedDataIntoAdapter(query)
        }
    }

    private fun loadSearchedDataIntoAdapter(query: String) {
        if (mBinding.rv.adapter != searchResultsAdapter){
            setRvAdapterAsSearchedAdapter()
        }
        val allItems = gamesPagedAdapter.snapshot().filter { it?.name?.contains(query, ignoreCase = true) == true }
        searchResultsAdapter.submitList(allItems)
    }

    private fun resetSearch() {
        searchResultsAdapter.submitList(emptyList())
        setRvAdapterAsPagedAdapter()
    }

    override fun onBackPressed() {
        if (!mSearchView.isIconified) {
            resetSearch()
            mSearchView.onActionViewCollapsed()
        } else {
            super.onBackPressed()
        }
    }
}