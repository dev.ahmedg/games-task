package com.thirdwayv.gameslist.view

import androidx.lifecycle.ViewModel
import com.thirdwayv.gameslist.domain.GetGamesUseCase


class GamesListViewModel(getGamesUseCase: GetGamesUseCase):ViewModel() {


    val allGamesPaged = getGamesUseCase()

}