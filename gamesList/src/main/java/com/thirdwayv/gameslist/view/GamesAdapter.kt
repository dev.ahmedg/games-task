package com.thirdwayv.gameslist.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.thirdwayv.base.baseEntities.BaseAdapter
import com.thirdwayv.data.network.entities.GameItem
import com.thirdwayv.gameslist.databinding.ItemGameBinding

class GamesAdapter(private val onItemClick:(GameItem)->Unit): BaseAdapter<GameItem>() {


    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return ItemGameBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        val item = getItem(position)
        (binding as ItemGameBinding).apply {
            model = item
            onClick = View.OnClickListener {
                onItemClick(item)
            }
        }
    }
}