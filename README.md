# ThirdWayv games
This repository is based on a modularized MVVM and clean architectures with the usage of Use cases pattern which has the following benifits :
 1. Avoiding god objects (viewModels) that deals with both presentation logic and dataflow logic.
 2. Reusing of dataflow logic across different ViewModels.

## Modules
 1. Base module : contains superclasses for the common components (activity , fragment ,...)
 2. Data module : contains both persistance and network layers , repositories are here to be sharable cross all features
 3. Features modules : contain view , view models and usecase (Splash - Favourite genres - Games list - game details)
 4. app module : contains the app class and the upper di module .
 
 ## Stack
 1. Kotlin.
 2. Coroutines with flow.
 3. Live data.
 4. MVVM with clean arch (Abstract layers) , use cases and repositories
 5. Room database
 6. Retrofit
 
 ## Unit and instrumentation tests
 Mainly mockito and other mocking libs are used to provide fake depencies in unit testing.

 1. Junit4 
 2. Junit Asserttions
 4. Mockito.
 5. kluent.
 
 ## Top tools
 1. Paging 3 lib with mediator usage to handle caching. 
 2. Koin - DI - used for locating dependencies cross all the app.
 3. SDP library to support different screen dimens in a simple way.
 4. Mockito  .
 6. Dokka plugin - Generating documentation in (Html - Jdocs) and other formats
 7. Jacoco plugin - Generating unit and instrumentation tests coverage reports in xml and html.
 
### The source code is partially documented
