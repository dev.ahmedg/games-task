package com.thirdwayv.gamedetails.domain

import com.thirdwayv.data.other.GameSliderItem
import com.thirdwayv.data.other.ResultState
import com.thirdwayv.data.repository.games.GamesRepo
import com.thirdwayv.gamedetails.view.GameDetailsUiModel
import kotlinx.coroutines.flow.map


open class GetGameDetailsUseCase constructor(private val repository: GamesRepo) {

    suspend operator fun invoke(id:Int) = repository.getGameDetails(id).map {
        when(it){
            is ResultState.Success ->{
                val game = it.data
                val sliderItems = mutableListOf<GameSliderItem>()
                game?.screenShots?.let { screenshots->
                    sliderItems.addAll(screenshots.map { GameSliderItem(it, type = GameSliderItem.Type.SCREENSHOT) })
                }
                game?.trailers?.let { trailers->
                    sliderItems.addAll(trailers.map { GameSliderItem(trailerItem = it, type = GameSliderItem.Type.TRAILER) })
                }

                ResultState.Success(GameDetailsUiModel(game?.name,game?.description,game?.rating?.toFloat(),game?.released, sliderItems))
            }
            is ResultState.Loading -> {
                ResultState.Loading }
            is ResultState.Error ->{
                ResultState.Error(it.exception) }
            is ResultState.Empty ->{
                ResultState.Empty }
        }

    }

}