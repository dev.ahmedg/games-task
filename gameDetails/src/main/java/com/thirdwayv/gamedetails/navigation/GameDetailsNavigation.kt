package com.thirdwayv.gamedetails.navigation

import android.os.Bundle
import com.thirdwayv.base.baseEntities.BaseNavigationManager
import com.thirdwayv.gamedetails.view.GameDetailsActivity

class GameDetailsNavigation : BaseNavigationManager() {

    fun startDetailsScreen(id:Int){
        startWithBundle(GameDetailsActivity::class.java, Bundle().apply {
            putInt(KEY_ID,id)
        })
    }
}