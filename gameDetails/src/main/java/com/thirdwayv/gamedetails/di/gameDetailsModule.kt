package com.thirdwayv.gamedetails.di



import com.thirdwayv.gamedetails.domain.GetGameDetailsUseCase
import com.thirdwayv.gamedetails.view.GameDetailsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val gameDetailsModule = module {


    factory { GetGameDetailsUseCase(get()) }

    viewModel {(id:Int) -> GameDetailsViewModel(id,get())}
}