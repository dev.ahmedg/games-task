package com.thirdwayv.gamedetails.view

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.MediaController
import androidx.core.view.isVisible
import com.smarteist.autoimageslider.SliderViewAdapter
import com.thirdwayv.base.utils.bindImage
import com.thirdwayv.data.other.GameSliderItem
import com.thirdwayv.gamedetails.databinding.ItemGameSliderBinding


class GameSliderAdapter : SliderViewAdapter<GameSliderAdapter.SliderAdapterVH>() {


    private var mSliderItems = mutableListOf<GameSliderItem>()

    override fun getCount(): Int = mSliderItems.size

    fun updateItems(sliderItems: List<GameSliderItem>) {
        mSliderItems = sliderItems.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup): SliderAdapterVH {
        return SliderAdapterVH(ItemGameSliderBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(viewHolder: SliderAdapterVH, position: Int) {
        val item = mSliderItems[position]
        viewHolder.binding.apply {
            val isScreenShot = item.type == GameSliderItem.Type.SCREENSHOT

            imageView.isVisible = isScreenShot
            videoView.isVisible = !isScreenShot

            if (isScreenShot){
                bindImage(imageView,item.screenShotItem?.image)
            }else{
                val mediaController = MediaController(root.context).apply { setAnchorView(videoView) }
                videoView.setVideoURI(Uri.parse(item.trailerItem?.data?.jsonMember480))
                videoView.setMediaController(mediaController)
            }
        }
    }


    class SliderAdapterVH(val binding: ItemGameSliderBinding) : SliderViewAdapter.ViewHolder(binding.root)
}