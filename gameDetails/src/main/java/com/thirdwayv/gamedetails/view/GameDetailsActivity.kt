package com.thirdwayv.gamedetails.view

import com.thirdwayv.base.baseEntities.BaseActivity
import com.thirdwayv.base.baseEntities.BaseNavigationManager.Companion.KEY_ID
import com.thirdwayv.data.other.ResultState
import com.thirdwayv.gamedetails.R
import com.thirdwayv.gamedetails.databinding.ActivityGameDetailsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class GameDetailsActivity : BaseActivity<ActivityGameDetailsBinding>() {


    private val mViewModel by viewModel<GameDetailsViewModel>{
        parametersOf(intent.getIntExtra(KEY_ID,0))
    }

    private val sliderAdapter = GameSliderAdapter()

    override fun getLayoutRes(): Int = R.layout.activity_game_details

    override fun getToolbarTitle(): Any = R.string.game_details

    override fun onViewAttach() {
        super.onViewAttach()
        setUpViews()
        observeData()
    }

    private fun setUpViews() {
       mBinding.sliderView.setSliderAdapter(sliderAdapter)
    }

    private fun observeData() {
        mViewModel.detailsLiveData.observe(this){
            when(it){
                is ResultState.Success -> populateData(it.data!!)
                is ResultState.Loading ->showLoading(mBinding.stateful)
                is ResultState.Error -> showError(mBinding.stateful,it.exception.message){mViewModel.getData()}
                else -> {}
            }
        }
    }

    private fun populateData(data: GameDetailsUiModel) {
        showContent(mBinding.stateful)
        mBinding.tvTitle.text = data.name
        mBinding.tvDescription.text = data.desc
        mBinding.tvReleaseDate.text = data.releasedDate
        mBinding.ratingBar.rating = data.rating!!
        sliderAdapter.updateItems(data.sliderItems!!)

    }
}