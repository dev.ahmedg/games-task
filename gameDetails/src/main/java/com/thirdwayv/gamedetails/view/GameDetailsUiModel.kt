package com.thirdwayv.gamedetails.view

import com.thirdwayv.data.other.GameSliderItem

open class GameDetailsUiModel(val name :String? , val desc:String? , val rating : Float? , val releasedDate:String? , val sliderItems : List<GameSliderItem>?)
