package com.thirdwayv.gamedetails.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thirdwayv.data.other.ResultState
import com.thirdwayv.gamedetails.domain.GetGameDetailsUseCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class GameDetailsViewModel(private val id:Int , private val getGameDetailsUseCase: GetGameDetailsUseCase): ViewModel() {


    private val _detailsLiveData = MutableLiveData<ResultState<GameDetailsUiModel>>()
    val detailsLiveData : LiveData<ResultState<GameDetailsUiModel>> = _detailsLiveData

    init {
        getData()
    }

    fun getData() {
        viewModelScope.launch {
            getGameDetailsUseCase(id).collect {
                _detailsLiveData.value = it
            }
        }
    }
}