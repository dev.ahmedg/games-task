package com.thirdwayv.base.events

/**
 * Event to use with EventBus to handle opening FavGenre screen from other module that does not depend on fav genre module
 */
class ShouldOpenFavGenresScreenEvent