package com.thirdwayv.base.baseEntities

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagingData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.thirdwayv.base.R


/**
 * Utilize creating and binding data into viewHolder
 */
abstract class BasePagedListAdapter<T:BaseEntity>(diffCallback: DiffUtil.ItemCallback<T> = object : DiffUtil.ItemCallback<T>() {

    override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem.entityId() == newItem.entityId()
    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T) = oldItem == newItem

})
    : PagingDataAdapter<T, RecyclerView.ViewHolder>(diffCallback) {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return getViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as BaseViewHolder<*>).binding.root.setTag(R.string.position, position)

        bind(holder.binding, position)

    }


    open fun getViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        BaseViewHolder(createBinding(parent, viewType))


    abstract fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding


    protected abstract fun bind(binding: ViewDataBinding, position: Int)


}