package com.thirdwayv.base.utils

import androidx.paging.LoadState
import androidx.paging.PagingDataAdapter


/**
 * Handle Pagination first page states
 *
 * Managing (loading , error , success) states of PaginationAdapter
 * @param onLoading the block to execute on initial loading.
 * @param onEmpty the block to execute on initial empty.
 * @param onError the block to execute on initial error.
 * @param resetStates the block to execute between states to reset.
 */
fun PagingDataAdapter<*,*>.handleInitialState(
    onLoading: () -> Unit,
    onEmpty: () -> Unit,
    onError: (Throwable) -> Unit,
    resetStates: () -> Unit
){
    addLoadStateListener {loadState->
        if (itemCount < 1) {

            val refreshState = loadState.refresh
            when{
                refreshState is LoadState.NotLoading && loadState.append.endOfPaginationReached ->  onEmpty()

                refreshState is LoadState.Error -> onError(refreshState.error)

                refreshState is LoadState.Loading -> onLoading()
            }

        } else {
            resetStates()
        }

    }
}