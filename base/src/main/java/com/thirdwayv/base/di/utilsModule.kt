package com.thirdwayv.base.di

import com.thirdwayv.base.utils.NetworkHelper
import org.koin.dsl.module

/**
 * Koin utils module
 */
val utilsModule = module {


    single { NetworkHelper() }
}
